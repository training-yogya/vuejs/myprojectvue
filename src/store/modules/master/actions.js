import api from "../../../services/api";

const actions = {
  getData: ({ commit }) => {
    return new Promise((resolve, reject) => {
      api.get("/training").then(
        response => {
          const res = response.data;
          resolve(response);
          if (res.status) {
            commit("SET_RESDATA", res.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deleteData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.delete("/training/" + state.idData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  insertData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.post("/training", state.dataMaster).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  updateData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.put("/training/" + state.idData, state.dataMaster).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

export default actions;
