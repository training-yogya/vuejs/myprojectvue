import api from "../../../services/api";

const actions = {
  inputBarang: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.post("/training", state.tempBarang).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
};

export default actions;
