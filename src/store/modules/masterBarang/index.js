import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
import state from "./state";

const masterBarang = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

export default masterBarang;
