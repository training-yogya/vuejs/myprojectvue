import Vue from "vue";
import VueRouter from "vue-router";
import Cookies from "js-cookie";
import Clean from "@/views/layouts/CleanLayout.vue";
import Home from "@/views/layouts/HomeLayout.vue";

Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Root",
      component: Clean,
      redirect: "login",
      children: [
        {
          path: "login",
          name: "login",
          component: () => import("@/views/pages/login"),
          meta: {
            guest: true
          }
        },
        {
          path: "",
          name: "Dashboard",
          redirect: "dashboard",
          component: Home,
          children: [
            {
              path: "dashboard",
              name: "Home Dashboard",
              component: () => import("@/views/pages/dashboard/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "master",
              name: "Master",
              component: () => import("@/views/pages/master/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "karyawan",
              name: "karyawan",
              component: () => import("@/views/pages/karyawan"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "barang",
              name: "Barang",
              component: () => import("@/views/pages/masterBarang/index.vue"),
              meta: {
                requireAuth: true
              }
            }
          ]
        }
      ]
    },
    {
      path: "*",
      name: "Not Found",
      component: () => import("@/views/Error/NotFound.vue")
    }
  ]
});
router.beforeEach((to, from, next) => {
  const token = Cookies.get("token");
  if (to.matched.some(record => record.meta.requireAuth)) {
    if (token === null || token === undefined) {
      next("/login");
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (token === null || token === undefined) {
      next();
    } else {
      next("/dashboard");
    }
  }
});
const DEFAULT_TITLE = "MyTraining";

router.afterEach(to => {
  Vue.nextTick(() => {
    document.title = process.env.VUE_APP_TITLE + " - " + to.name || DEFAULT_TITLE;
  });
});
export default router;
