export const sidebar = [
  {
    icon: "mdi-monitor",
    name: "Dashboard",
    to: "/dashboard",
    color: "#000000"
  },
  {
    icon: "mdi-account-multiple",
    name: "Master",
    to: "/master",
    color: "#000000"
  },
  {
    icon: "mdi-account",
    name: "Karyawan",
    to: "/karyawan",
    color: "#000000"
  },
  {
    icon: "mdi-airplane",
    name: "Barang",
    to: "/barang",
    color: "#000000"
  }
];
