import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "./plugins/VeeValidate";
import { extend } from "vee-validate";
import * as rules from "vee-validate/dist/rules";
import VueApexCharts from "vue-apexcharts";
import VueSweetalert2 from "vue-sweetalert2";

// If you don't need the styles, do not connect
import "sweetalert2/dist/sweetalert2.min.css";

Vue.use(VueSweetalert2);

Vue.config.productionTip = false;

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

Vue.use(VueApexCharts);

Vue.component("apexchart", VueApexCharts);
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
